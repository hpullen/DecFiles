# EventType: 15196032
# NickName: Lb_LambdacDstars12700,Lc_pKpi,Dstars12700_D0K=DecProdCut
# Descriptor: [Lambda_b0 -> (D_s*- -> (anti-D0 -> K+ pi-) K-) (Lambda_c+ -> p+ K- pi+)]cc
#
# Documentation:
#   Decay Lambda_b0 -> Lambda_c+ D*_s1(2700)- with D*_s1(2700)- --> D~0 K-, D~0 --> K+ pi-
#   Daughters in LHCb Acceptance
# EndDocumentation
#
# Cuts: DaughtersInLHCb
# CPUTime: < 1 min
#
# ###             NAME    GEANTID  PDGID  CHARGE MASS(GeV) TLIFE(s)  EVTGENNAME  PYTHIAID  MAXWIDTH(GeV)
# ParticleValue: "D*_s+    433      433   +1.0   2.708    5.488e-24    D_s*+      433      0.120",     "D*_s-  -433  -433  -1.0  2.708  5.488e-24  D_s*-  -433  0.120"
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Alessio Piucci
# Email: alessio.piucci@cern.ch
# Date: 20180915
#

Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-

Alias      MyDstars1+          D_s*+
Alias      Myanti-Dstars1-     D_s*-
ChargeConj MyDstars1+          Myanti-Dstars1-

Alias      MyD0          D0
Alias      Myanti-D0     anti-D0
ChargeConj MyD0          Myanti-D0

#
Decay Lambda_b0sig
  1.000    MyLambda_c+    Myanti-Dstars1-            PHSP;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyLambda_c+
  1.000   p+ K- pi+                  PHSP;
Enddecay
CDecay Myanti-Lambda_c-

Decay Myanti-Dstars1-
  1.000   Myanti-D0  K-                  PHSP;
Enddecay
CDecay MyDstars1+

Decay Myanti-D0
  1.000   K+ pi-                  PHSP;
Enddecay
CDecay MyD0

#
End
#
