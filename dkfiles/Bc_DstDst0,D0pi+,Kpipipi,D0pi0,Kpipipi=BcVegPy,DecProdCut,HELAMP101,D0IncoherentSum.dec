# EventType: 14299401
#
# Descriptor: [ B_c+ => (D*(2010)+ => (D0 -> K- pi+ pi- pi+) pi+) (D*(2007)~0 => (D~0 => K+ pi- pi+ pi-) pi0) ]cc
#
# NickName: Bc_DstDst0,D0pi+,Kpipipi,D0pi0,Kpipipi=BcVegPy,DecProdCut,HELAMP101,D0IncoherentSum
#
# Production: BcVegPy
#
# Cuts: BcDaughtersInLHCb
# CutsOptions: NeutralThetaMin 0. NeutralThetaMax 10.
#
# Documentation: Daughters in LHCb Acceptance. Loose angular acceptance on neutrals.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: A. Tully
# Email: alison.tully@cern.ch
# Date: 20170815
#
Alias      MyD*+    D*+
Alias      MyD*-    D*-
ChargeConj MyD*+    MyD*-

Alias      Myanti-D*0    anti-D*0
Alias      MyD*0    D*0
ChargeConj Myanti-D*0    MyD*0

Alias      MyD0        D0
Alias      Myanti-D0   anti-D0
ChargeConj MyD0        Myanti-D0

Alias      Mya_1+      a_1+
Alias      Mya_1-      a_1-
ChargeConj Mya_1+      Mya_1-

Alias      MyK*0       K*0
Alias      Myanti-K*0  anti-K*0
ChargeConj MyK*0       Myanti-K*0

Alias      MyK_1-      K_1-
Alias      MyK_1+      K_1+
ChargeConj MyK_1-      MyK_1+

Alias      Myomega     omega
ChargeConj Myomega     Myomega

Alias      Myrho0      rho0
ChargeConj Myrho0      Myrho0


Decay B_c+sig
  1.000     MyD*+       Myanti-D*0       SVV_HELAMP 1.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay B_c-sig

Decay MyD*+
  1.000     MyD0    pi+     VSS;
Enddecay
CDecay MyD*-

Decay Myanti-D*0
  1.000     Myanti-D0    pi0    VSS;
Enddecay
CDecay MyD*0

Decay MyD0
0.0360      Mya_1+       K-                       SVS;
0.0097      Myanti-K*0   Myrho0                   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
0.0002      Myanti-K*0   Myomega                  SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
0.0038      MyK_1-       pi+                      SVS;
0.0150      Myanti-K*0   pi+   pi-                PHSP;
0.0068      K-           pi+   Myrho0             PHSP;
0.0007      K-           pi+   Myomega            PHSP;
0.0074      K-           pi+   pi+       pi-      PHSP;
Enddecay
CDecay Myanti-D0
#
# Total a_1+ = 0.492
#
Decay Mya_1+ 
  1.0000      Myrho0      pi+            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay 
Decay Mya_1-
  1.0000      Myrho0      pi-            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
#
#  Total K*0 = 0.6657
#
Decay MyK*0
  1.0000      K+        pi-            VSS;
Enddecay
CDecay Myanti-K*0
#
#  Total omega = 0.0221
#
Decay Myomega
  1.0000      pi+       pi-            VSS;
Enddecay
Decay Myrho0
  1.0000      pi+       pi-            VSS;
Enddecay
#
# Total K_1- = 0.3578
#
Decay MyK_1+
  0.1400      Myrho0       K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0710      MyK*0        pi+         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0024      Myomega      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1444      K+    pi+    pi-         PHSP;
Enddecay
Decay MyK_1-
  0.1400      Myrho0       K-          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0710      Myanti-K*0   pi-         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0024      Myomega      K-          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1444      K-    pi-    pi+         PHSP;
Enddecay


#
End
