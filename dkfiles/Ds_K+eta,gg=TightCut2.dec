# EventType: 23101431
#
# Descriptor: [D_s- -> K- ( eta -> gamma gamma )]cc 
#
# NickName: Ds_K+eta,gg=TightCut2
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D_s+ => ^K+ ( eta => gamma gamma )]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'  : ' inAcc & piCuts',
#     '[D_s+]cc'   : 'Dcuts' }
# tightCut.Preambulo += [
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' , 
#     'piCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',
#     'Dcuts = (GPT > 1000 * MeV)' ]
# EndInsertPythonCode
#
# Documentation: Inclusive production of Ds-. Ds- forced to decay to K- eta as phase space, then eta to gamma gamma. Used to look at signal when a single gamma converts into e+ e- (Geant) inside LHCb. Generator level cuts on pi and D meson. (v2) Changed arrow type -> to => with respect to 23101431
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Tom Hadavizadeh
# Email: tom.hadavizadeh@cern.ch
# Date: 20190108
#
Alias      MyEta        eta
ChargeConj MyEta        MyEta
#
Decay D_s-sig
  1.00   K-   MyEta     PHSP;
Enddecay
CDecay D_s+sig
#
Decay MyEta
  1.00  gamma gamma     PHSP;
Enddecay
#
End
#
