# EventType: 10334101
#
# Descriptor: [psi(2S) => ( Lambda(1520)0 => p+ K- ) ( anti-Lambda0 => p~- pi+ )]CC 
#
# NickName: incl_b=psi2S,LstLambda,pKppi,InAcc
#
# Cuts: DaughtersInLHCb
# FullEventCuts: LoKi::FullGenEventCut/b2psi2SFilter
# Sample: RepeatDecay.Inclusive
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "b2psi2SFilter" )
# SignalFilter = Generation().b2psi2SFilter
# SignalFilter.Code = " has(isB2ccTcuts)"
# SignalFilter.Preambulo += [
#  "from GaudiKernel.SystemOfUnits import MeV, GeV, mrad",
#  "inAcc = (in_range(  0.010 , GTHETA , 0.400 ))",
#  "isB2cc = ((GDECTREE('(Beauty & LongLived) --> psi(2S) ...')))",
#  "ppcuts = (GINTREE( (('p+'  == GID ) & (GPT > 250 * MeV) & inAcc) ) )",
#  "pmcuts = (GINTREE( (('p~-' == GID ) & (GPT > 250 * MeV) & inAcc) ) )",
#  "pipcuts = (GINTREE( (('pi+'  == GID ) & (GPT > 250 * MeV) & inAcc) ) )",
#  "pimcuts = (GINTREE( (('pi-'  == GID ) & (GPT > 250 * MeV) & inAcc) ) )",
#  "isB2ccTcuts = (isB2cc & ppcuts & pmcuts & pipcuts & pimcuts)"
#    ]
# EndInsertPythonCode
#
# Documentation: psi(2S) decay to Lst Lambda, keep only particles in acceptance
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Andrii Usachov
# Email: andrii.usachov@cern.ch
# Date: 20190220
#
Alias      MyLst         Lambda(1520)0
Alias      MyantiLst     anti-Lambda(1520)0
Alias	   MyLambda      Lambda0
Alias	   MyantiLambda  anti-Lambda0
ChargeConj MyLst     MyantiLst
ChargeConj MyLambda  MyantiLambda
#
Decay psi(2S)
  0.500    MyLst   MyantiLambda      PHSP;
  0.500    MyantiLst   MyLambda      PHSP;
Enddecay
#
Decay MyLst
  1.000  p+      K-    PHSP; 
Enddecay
CDecay MyantiLst
#
Decay MyLambda
  1.000  p+	 pi-    PHSP;
Enddecay
CDecay MyantiLambda
#
End
#

