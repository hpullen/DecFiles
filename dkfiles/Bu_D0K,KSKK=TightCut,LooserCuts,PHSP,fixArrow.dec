# EventType: 12165149
#
# Descriptor: [B+ -> K+ (anti-D0 -> (K_S0 -> pi+ pi-) K+ K-)]cc
#
# NickName: Bu_D0K,KSKK=TightCut,LooserCuts,PHSP,fixArrow
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B+ => ^(D~0 => ^(KS0 => ^pi+ ^pi-) ^K+ ^K-) ^K+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'goodB        = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter)',
#     'goodD        = (GP > 10000 * MeV) & (GPT > 500 * MeV)',
#     'goodKS       = (GP > 4000 * MeV) & (GPT > 250 * MeV)',
#     'goodDDaugK   = (GNINTREE (("K+"  == GABSID) & (GP > 1000 * MeV) & inAcc, 1) > 1.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 1750 * MeV) & inAcc, 1) > 1.5)',
#     'goodBachK    = (GNINTREE (("K+"  == GABSID) & (GP > 4000 * MeV) & (GPT > 400 * MeV) & inAcc, 1) > 0.5)'
# ]
# tightCut.Cuts      =    {
#     '[B+]cc'         : 'goodB  & goodBachK',
#     '[D0]cc'         : 'goodD  & goodDDaugK',
#     '[KS0]cc'        : 'goodKS & goodKsDaugPi',
#     '[pi+]cc'        : 'inAcc'
#     }
# EndInsertPythonCode
#
# Documentation: B decays to D0 K, D0 decays to KS K+ K- without resonance, KS decays to pi+ pi-, decay products in acceptance and looser tight cuts, including B decays with radiated photons 
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Mikkel Bjoern
# Email: mikkel.bjoern@cern.ch
# Date: 20190215
# CPUTime: <1min
#
Alias      MyD0      D0
Alias      Myanti-D0 anti-D0
Alias      MyK_S0    K_S0
ChargeConj MyD0      Myanti-D0
ChargeConj MyK_S0    MyK_S0
#
Decay B+sig
  1.000    Myanti-D0 K+ PHSP;
Enddecay
CDecay B-sig
#
Decay Myanti-D0
  1.000    MyK_S0 K+ K- PHSP;
Enddecay
CDecay MyD0
#
Decay MyK_S0
  1.000    pi+ pi-      PHSP;
Enddecay
#
End

