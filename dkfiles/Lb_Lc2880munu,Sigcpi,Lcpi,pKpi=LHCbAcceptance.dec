# EventType: 15676003
#
# Descriptor: [Lambda_b0 -> (Lambda_c(2625)+ -> (Sigma_c0 -> (Lambda_c+ -> p+ K- pi+) pi-) pi+) mu- anti-nu_mu]cc
#
# NickName: Lb_Lc2880munu,Sigcpi,Lcpi,pKpi=LHCbAcceptance
#
# Cuts: LHCbAcceptance
#
# Documentation: Lb decaying to Lambda_c(2880) mu- anti-nu_mu. Lc2880 exists in evtgen, but may not be used with Lb2Baryonlnu
# so we use a Lc2625 with mass set to the Lc2880 pdg mass. Lc2880 is then forced to either Sigma_c0 pi+ or Sigma_c++ pi-, with Sigma_c decaying to Lc pi.
#
# EndDocumentation
#
# CPUTime: < 1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Scott Ely
# Email: seely@syr.edu
# Date: 20180816
#
# ParticleValue: "Lambda_c(2625)+ 104124   104124   1.0   2.88163 1.134849e-22  Lambda_c(2625)+ 0 1.0e-004", "Lambda_c(2625)~- -104124   -104124   -1.0   2.88163 1.134849e-22  anti-Lambda_c(2625)- 0 1.0e-004"
#
Alias		MyLambda_c+			Lambda_c+
Alias		MyAntiLambda_c-			anti-Lambda_c-
ChargeConj	MyLambda_c+			MyAntiLambda_c-
#
Alias		MyLambda_c(2625)+		Lambda_c(2625)+
Alias		MyAntiLambda_c(2625)-		anti-Lambda_c(2625)-
ChargeConj	MyLambda_c(2625)+		MyAntiLambda_c(2625)-
#
Alias		MySigma_c++			Sigma_c++
Alias		MyAntiSigma_c--			anti-Sigma_c--
ChargeConj	MySigma_c++			MyAntiSigma_c--
#
Alias		MySigma_c0			Sigma_c0
Alias		MyAntiSigma_c0			anti-Sigma_c0
ChargeConj	MySigma_c0			MyAntiSigma_c0
#
Decay Lambda_b0sig
 1.00	 MyLambda_c(2625)+	mu-	anti-nu_mu		Lb2Baryonlnu 1 1 1 1;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c(2625)+
 0.5  MySigma_c++	pi-	PHSP;
 0.5  MySigma_c0	pi+	PHSP;
Enddecay
CDecay MyAntiLambda_c(2625)-
#
Decay MySigma_c++
 1.0	MyLambda_c+	pi+	PHSP;
Enddecay
CDecay MyAntiSigma_c--
#
Decay MySigma_c0
 1.0	MyLambda_c+	pi-	PHSP;
Enddecay
CDecay MyAntiSigma_c0
#
Decay MyLambda_c+
 1.0     p+	K-	pi+	PHSP;
Enddecay
CDecay MyAntiLambda_c-
#
End
