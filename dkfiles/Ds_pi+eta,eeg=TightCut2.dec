# EventType: 23123231
#
# Descriptor: [D_s- -> pi- (eta -> e+ e- gamma)]cc 
#
# NickName: Ds_pi+eta,eeg=TightCut2
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D_s+ => ^pi+ ( eta => ^e+ ^e- gamma )]CC'
# tightCut.Cuts      =    {
#     '[e+]cc'  : ' inAcc & eCuts',
#     '[pi+]cc' : ' inAcc & piCuts',
#     '[D_s+]cc': ' Dcuts ' }
# tightCut.Preambulo += [
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' , 
#     'eCuts = ( (GPT > 50 * MeV) & ( GP > 600 * MeV))',
#     'piCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',
#     'Dcuts = (GPT > 1000 * MeV)' ]
# EndInsertPythonCode
#
# Documentation: D_s+ forced to decay to pi+ eta, then eta to (e+ e- gamma) with generator level cuts. (v2) Changed arrow type -> to => with respect to 23123220
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Tom Hadavizadeh 
# Email: tom.hadavizadeh@cern.ch
# Date: 20190108
#
Alias      MyEta        eta
ChargeConj MyEta        MyEta
#
Decay D_s-sig
  1.00    pi- MyEta           PHSP;
Enddecay
CDecay D_s+sig
#
Decay MyEta
  1.00    e+  e- gamma        PHSP;
Enddecay
#
End
#
