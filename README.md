# Deadline for next release

The release plan with deadline for DecFiles over coming months is:

  1. DecFiles v30r30 with deadline on Friday 22th March 2019 at 14:00.
  1. DecFiles v30r31 with deadline on Tuesday 7th April 2019 at 14:00.
  1. DecFiles v30r32 with deadline on Friday 26th April 2019 at 14:00.


Usually plan is to release on Monday or Tuesday after deadline. Merge requests created after deadline are not guaranteed to be accepted for this release.

# Guide for submitting decay file
Please follow [Contribution guide](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/blob/master/CONTRIBUTING.md) on top of the page for instructions on how to prepare, test and commit decay file.

# Overiew of existing decay files
A list of existing decay files, event numbers, and related comments can be found here: [http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/](http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/).
