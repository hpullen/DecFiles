"Event Pattern"        421 -321 211 211 -211

# LHCb Preliminary Cabbibo Favoured K3pi model 
# Mint v3r0 model - missing key decay modes and uses wrong barrier factors 
# but should reproduce signal reasonably well
# 11/11/2014

D0->K(0)*(1430)bar0(->K-,pi+),NonResV0(->pi+,pi-)_Re  0 18.6562 0.1
D0->K(0)*(1430)bar0(->K-,pi+),NonResV0(->pi+,pi-)_Im  0 -5.3722 0.1
D0->K(1)(1270)bar-(->NonResS0(->pi+,pi-),K-),pi+_Re 0 -3.92383  0.1
D0->K(1)(1270)bar-(->NonResS0(->pi+,pi-),K-),pi+_Im 0 -0.426698 0.1
D0->K(1)(1270)bar-(->rho(770)0(->pi+,pi-),K-),pi+_Re  0 4.19341 0.1
D0->K(1)(1270)bar-(->rho(770)0(->pi+,pi-),K-),pi+_Im  0 -0.123759 0.1
D0->K(1)(1270)bar-[D](->NonResV0(->pi+,pi-),K-),pi+_Re  0 -211.311  0.1
D0->K(1)(1270)bar-[D](->NonResV0(->pi+,pi-),K-),pi+_Im  0 17.7947 0.1
D0->K(1)(1270)bar-[D](->rho(770)0(->pi+,pi-),K-),pi+_Re 0 97.3684 0.1
D0->K(1)(1270)bar-[D](->rho(770)0(->pi+,pi-),K-),pi+_Im 0 -17.6525  0.1
D0->K(1)(1400)bar-(->K*(892)bar0(->pi+,K-),pi-),pi+_Re  0 -0.994741 0.1
D0->K(1)(1400)bar-(->K*(892)bar0(->pi+,K-),pi-),pi+_Im  0 0.19938 0.1
D0->K*(1680)bar0(->K-,pi+),NonResS0(->pi+,pi-)_Re 0 24.9385 0.1
D0->K*(1680)bar0(->K-,pi+),NonResS0(->pi+,pi-)_Im 0 -35.2421  0.1
D0->K*(892)bar0(->K-,pi+),rho(770)0(->pi+,pi-)_Re 0 -0.291893 0.1
D0->K*(892)bar0(->K-,pi+),rho(770)0(->pi+,pi-)_Im 0 6.07846 0.1
D0->K-,pi+,pi+,pi-_Re 0 -0.91792  0.1
D0->K-,pi+,pi+,pi-_Im 0 -15.6195  0.1
D0->NonResS0(->K-,pi+),rho(770)0(->pi+,pi-)_Re  0 -8.05159  0.1
D0->NonResS0(->K-,pi+),rho(770)0(->pi+,pi-)_Im  0 -6.96592  0.1
D0->NonResV0(->K-,pi+),rho(770)0(->pi+,pi-)_Re  0 -27.5534  0.1
D0->NonResV0(->K-,pi+),rho(770)0(->pi+,pi-)_Im  0 6.68201 0.1
D0->a(1)(1260)+(->rho(770)0(->pi+,pi-),pi+),K-_Re 0 -2.56572  0.1
D0->a(1)(1260)+(->rho(770)0(->pi+,pi-),pi+),K-_Im 0 13.1752 0.1
D0->rho(770)0(->pi+,pi-),kappabar0(->K-,pi+)_Re 0 -0.541795 0.1
D0->rho(770)0(->pi+,pi-),kappabar0(->K-,pi+)_Im 0 -0.509875 0.1
D0->sigma20(->pi+,pi-),K*(892)bar0(->K-,pi+)_Re 0 -3.02784  0.1
D0->sigma20(->pi+,pi-),K*(892)bar0(->K-,pi+)_Im 0 2.93637 0.1
D0[D]->K*(892)bar0(->K-,pi+),rho(770)0(->pi+,pi-)_Re  0 -0.326979 0.1
D0[D]->K*(892)bar0(->K-,pi+),rho(770)0(->pi+,pi-)_Im  0 -5.52726  0.1
D0[D]->rho(1450)0(->pi+,pi-),K*(892)bar0(->K-,pi+)_Re 0 7.27294 0.1
D0[D]->rho(1450)0(->pi+,pi-),K*(892)bar0(->K-,pi+)_Im 0 17.526  0.1
D0[P]->NonResV0(->K-,pi+),rho(770)0(->pi+,pi-)_Re 0 -2.13078  0.1
D0[P]->NonResV0(->K-,pi+),rho(770)0(->pi+,pi-)_Im 0 -2.01235  0.1