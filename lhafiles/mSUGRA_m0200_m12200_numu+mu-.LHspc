# SUSY Les Houches Accord 2.beta - MSSM spectrum + Decays
# SPheno v3beta18
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101
# in case of problems send email to porod@ific.uv.es
# Created: 26.01.2009,  18:46
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v3beta18       # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    4    # MSSM with explicit R-parity violation
Block MINPAR  # Input parameters
    3    1.00000000E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block SMINPUTS  # SM parameters
         1     1.27934000E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16639000E-05  # G_mu [GeV^-2]
         3     1.17200000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.25000000E+00  # m_b(m_b), MSbar
         6     1.72700000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
Block EXTPAR  # 
   0    4.18897231E+02  # scale Q where the parameters below are defined
   1    8.05974667E+01  # M_1
   2    1.52744719E+02  # M_2
   3    4.75262075E+02  # M_3
  11   -4.14309339E+02  # A_t
  12   -6.64521198E+02  # A_b
  13   -2.21693051E+02  # A_l
  23    2.91938489E+02  # mu 
  31    2.40367456E+02  # M_(L,11)
  32    2.40363651E+02  # M_(L,22)
  33    2.39290181E+02  # M_(L,33)
  34    2.12818165E+02  # M_(E,11)
  35    2.12809484E+02  # M_(E,22)
  36    2.10351895E+02  # M_(E,33)
  41    4.77332131E+02  # M_(Q,11)
  42    4.77330349E+02  # M_(Q,22)
  43    4.27588718E+02  # M_(Q,33)
  44    4.64463168E+02  # M_(U,11)
  45    4.64461341E+02  # M_(U,22)
  46    3.56053007E+02  # M_(U,33)
  47    4.62993186E+02  # M_(D,11)
  48    4.62991305E+02  # M_(D,22)
  49    4.59745798E+02  # M_(D,33)
Block gauge Q=  4.18897231E+02  # (SUSY scale)
   1    3.60627783E-01  # g'(Q)^DRbar
   2    6.47134426E-01  # g(Q)^DRbar
   3    1.10541515E+00  # g3(Q)^DRbar
Block Yu Q=  4.18897231E+02  # (SUSY scale)
  1  1     8.91996626E-06   # Y_u(Q)^DRbar
  2  2     3.56798616E-03   # Y_c(Q)^DRbar
  3  3     8.89157897E-01   # Y_t(Q)^DRbar
Block Yd Q=  4.18897231E+02  # (SUSY scale)
  1  1     1.94284420E-04   # Y_d(Q)^DRbar
  2  2     3.33059039E-03   # Y_s(Q)^DRbar
  3  3     1.41679133E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.18897231E+02  # (SUSY scale)
  1  1     2.88505456E-05   # Y_e(Q)^DRbar
  2  2     5.96535785E-03   # Y_mu(Q)^DRbar
  3  3     1.00340229E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.18897231E+02  # (SUSY scale)
  1  1    -5.67370897E+02   # A_u(Q)^DRbar
  2  2    -5.67367708E+02   # A_c(Q)^DRbar
  3  3    -4.14309339E+02   # A_t(Q)^DRbar
Block Ad Q=  4.18897231E+02  # (SUSY scale)
  1  1    -7.18422666E+02   # A_d(Q)^DRbar
  2  2    -7.18419663E+02   # A_s(Q)^DRbar
  3  3    -6.64521198E+02   # A_b(Q)^DRbar
Block Ae Q=  4.18897231E+02  # (SUSY scale)
  1  1    -2.23261123E+02   # A_e(Q)^DRbar
  2  2    -2.23255580E+02   # A_mu(Q)^DRbar
  3  3    -2.21693051E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.18897231E+02  # soft SUSY breaking masses at Q
   1    8.05974667E+01  # M_1
   2    1.52744719E+02  # M_2
   3    4.75262075E+02  # M_3
  31    2.40367456E+02  # M_(L,11)
  32    2.40363651E+02  # M_(L,22)
  33    2.39290181E+02  # M_(L,33)
  34    2.12818165E+02  # M_(E,11)
  35    2.12809484E+02  # M_(E,22)
  36    2.10351895E+02  # M_(E,33)
  41    4.77332131E+02  # M_(Q,11)
  42    4.77330349E+02  # M_(Q,22)
  43    4.27588718E+02  # M_(Q,33)
  44    4.64463168E+02  # M_(U,11)
  45    4.64461341E+02  # M_(U,22)
  46    3.56053007E+02  # M_(U,33)
  47    4.62993186E+02  # M_(D,11)
  48    4.62991305E+02  # M_(D,22)
  49    4.59745798E+02  # M_(D,33)
Block RVKAPPA Q=  4.18897231E+02  # bilinear RP parameters at Q
         1     4.01897312E-02  # epsilon_1
         2    -4.01897312E-02  # epsilon_2
         3     4.01897312E-02  # epsilon_3
Block RVSNVEV Q=  4.18897231E+02  # sneutrino vevs at Q
         1    -3.31108918E-03  # v_L_1
         2     3.46309456E-03  # v_L_2
         3    -3.17210374E-03  # v_L_3
Block SPhenoRP  # additional RP parameters
         4     1.90050979E-03  # Lambda_1 = v_d epsilon_1 + mu v_L1
         5     4.24757109E-02  # Lambda_2 = v_d epsilon_2 + mu v_L2
         6     4.24757109E-02  # Lambda_3 = v_d epsilon_3 + mu v_L3
         7     2.20328631E-03  # m^2_atm [eV^2]
         8     7.47139314E-05  # m^2_sol [eV^2]
         9     9.34919357E-01  # tan^2 theta_atm
        10     5.54102314E-01  # tan^2 theta_sol
        11     1.73766521E-03  # U_e3^2
        15     2.40990634E+01  # v_d
        16     2.40990634E+02  # v_u
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         4     1.20000000E+00  # m_c(m_c), MSbar
         5     4.25000000E+00  # m_b(m_b), MSbar
         6     1.72700000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.03838493E+01  # W+
        25     1.08433653E+02  # leightest neutral scalar
   1000016     2.30900723E+02  # 2nd neutral scalar
   1000014     2.32056480E+02  # 3rd neutral scalar
   1000012     2.32060576E+02  # 4th neutral scalar
        35     3.69253221E+02  # 5th neutral scalar
   2000016     2.30900723E+02  # leightest pseudoscalar
   2000014     2.32056480E+02  # 2nd pseudoscalar
   2000012     2.32060576E+02  # 3rd pseudoscalar
        36     3.68932082E+02  # 4th pseudoscalar
   1000015     2.10613954E+02  # leightest charged scalar
   2000013     2.17750894E+02  # 2nd charged scalar
   2000011     2.17779036E+02  # 3rd charged scalar
   1000011     2.45459987E+02  # 4th charged scalar
   1000013     2.45473326E+02  # 5th charged scalar
   2000015     2.48411428E+02  # 6th charged scalar
        37     3.77843130E+02  # 7th charged scalar
   1000001     4.99761860E+02  # ~d_L
   2000001     4.82864638E+02  # ~d_R
   1000002     4.93627063E+02  # ~u_L
   2000002     4.82307834E+02  # ~u_R
   1000003     4.99762742E+02  # ~s_L
   2000003     4.82859886E+02  # ~s_R
   1000004     4.93639864E+02  # ~c_L
   2000004     4.82291874E+02  # ~c_R
   1000005     4.45647135E+02  # ~b_1
   2000005     4.81982958E+02  # ~b_2
   1000006     3.39222238E+02  # ~t_1
   2000006     5.17285725E+02  # ~t_2
   1000021     4.97567056E+02  # ~g
        12     7.19510941E-14  # nu_1
        14     8.64402154E-12  # nu_2
        16     4.77284550E-11  # nu_3
   1000022     7.60008038E+01  # chi_10
   1000023     1.39715584E+02  # chi_20
   1000025     3.00847022E+02  # chi_30
   1000035     3.20410717E+02  # chi_40
       -11     5.10999060E-04  # e+
       -13     1.05658000E-01  # mu+
       -15     1.77700000E+00  # tau+
   1000024     1.38908272E+02  # chi_1+
   1000037     3.21808569E+02  # chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.15707598E-01   # alpha
Block Hmix Q=  4.18897231E+02  # Higgs mixing parameters
   1    2.91938489E+02  # mu
   2    9.76140707E+00  # tan[beta](Q)
   3    2.45062634E+02  # v(Q)
   4    1.39907582E+05  # m^2_A(Q)
Block staumix  # stau mixing matrix
   1  1     3.38654864E-01   # R_sta(1,1)
   1  2     9.40910667E-01   # R_sta(1,2)
   2  1    -9.40910667E-01   # R_sta(2,1)
   2  2     3.38654864E-01   # R_sta(2,2)
Block stopmix  # stop mixing matrix
   1  1     5.60657111E-01   # R_st(1,1)
   1  2     8.28048069E-01   # R_st(1,2)
   2  1    -8.28048069E-01   # R_st(2,1)
   2  2     5.60657111E-01   # R_st(2,2)
Block sbotmix  # sbottom mixing matrix
   1  1     9.63133763E-01   # R_sb(1,1)
   1  2     2.69022963E-01   # R_sb(1,2)
   2  1    -2.69022963E-01   # R_sb(2,1)
   2  2     9.63133763E-01   # R_sb(2,2)
Block Nmix  # neutralino mixing matrix
   1  1    -9.76332655E-01   # N(1,1)  
   1  2     8.44166077E-02   # N(1,2)  
   1  3    -1.87689408E-01   # N(1,3)  
   1  4     6.64911295E-02   # N(1,4)  
   2  1    -1.50697710E-01   # N(2,1)  
   2  2    -9.23853265E-01   # N(2,2)  
   2  3     3.08396409E-01   # N(2,3)  
   2  4    -1.69342849E-01   # N(2,4)  
   3  1     7.42326022E-02   # N(3,1)  
   3  2    -1.11384752E-01   # N(3,2)  
   3  3    -6.88688983E-01   # N(3,3)  
   3  4    -7.12594164E-01   # N(3,4)  
   4  1     1.36214055E-01   # N(4,1)  
   4  2    -3.56317862E-01   # N(4,2)  
   4  3    -6.28785993E-01   # N(4,3)  
   4  4     6.77577661E-01   # N(4,4)  
Block Umix  # chargino U mixing matrix
   1  1    -8.87686651E-01   # U(1,1)
   1  2     4.60448053E-01   # U(1,2)
   2  1     4.60448053E-01   # U(2,1)
   2  2     8.87686651E-01   # U(2,2)
Block Vmix  # chargino V mixing matrix
   1  1    -9.66969645E-01   # V(1,1)
   1  2     2.54891558E-01   # V(1,2)
   2  1     2.54891558E-01   # V(2,1)
   2  2     9.66969645E-01   # V(2,2)
DECAY   2000001     2.59293213E-01   # ~d_R
#    BR                NDA      ID1      ID2
     9.73562708E-01    2     1000022         1   # BR(~d_R -> chi_10 d)
     1.80773558E-02    2     1000023         1   # BR(~d_R -> chi_20 d)
     2.17796624E-03    2     1000025         1   # BR(~d_R -> chi_30 d)
     6.17343168E-03    2     1000035         1   # BR(~d_R -> chi_40 d)
DECAY   1000001     4.79471735E+00   # ~d_L
#    BR                NDA      ID1      ID2
     2.75920978E-02    2     1000022         1   # BR(~d_L -> chi_10 d)
     2.97887573E-01    2     1000023         1   # BR(~d_L -> chi_20 d)
     2.71308680E-03    2     1000025         1   # BR(~d_L -> chi_30 d)
     2.18715586E-02    2     1000035         1   # BR(~d_L -> chi_40 d)
     5.88816148E-01    2    -1000024         2   # BR(~d_L -> chi_1- u)
     6.06005432E-02    2    -1000037         2   # BR(~d_L -> chi_2- u)
     5.18992877E-04    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     2.60280719E-01   # ~s_R
#    BR                NDA      ID1      ID2
     9.69947152E-01    2     1000022         3   # BR(~s_R -> chi_10 s)
     1.92272326E-02    2     1000023         3   # BR(~s_R -> chi_20 s)
     2.19236380E-03    2     1000025         3   # BR(~s_R -> chi_30 s)
     6.13459189E-03    2     1000035         3   # BR(~s_R -> chi_40 s)
     2.49137213E-03    2    -1000024         4   # BR(~s_R -> chi_1- c)
DECAY   1000003     4.79385685E+00   # ~s_L
#    BR                NDA      ID1      ID2
     2.75935182E-02    2     1000022         3   # BR(~s_L -> chi_10 s)
     2.97875913E-01    2     1000023         3   # BR(~s_L -> chi_20 s)
     2.72092341E-03    2     1000025         3   # BR(~s_L -> chi_30 s)
     2.18831782E-02    2     1000035         3   # BR(~s_L -> chi_40 s)
     5.88776794E-01    2    -1000024         4   # BR(~s_L -> chi_1- c)
     6.06316770E-02    2    -1000037         4   # BR(~s_L -> chi_2- c)
     5.17996807E-04    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.19818276E+00   # ~b_1
#    BR                NDA      ID1      ID2
     5.06885263E-02    2     1000022         5   # BR(~b_1 -> chi_10 b)
     3.77150654E-01    2     1000023         5   # BR(~b_1 -> chi_20 b)
     7.30070098E-03    2     1000025         5   # BR(~b_1 -> chi_30 b)
     1.79210661E-02    2     1000035         5   # BR(~b_1 -> chi_40 b)
     4.39700917E-01    2    -1000024         6   # BR(~b_1 -> chi_1- t)
     1.07238132E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W-)
DECAY   2000005     5.39812048E-01   # ~b_2
#    BR                NDA      ID1      ID2
     3.96588211E-01    2     1000022         5   # BR(~b_2 -> chi_10 b)
     8.76288797E-02    2     1000023         5   # BR(~b_2 -> chi_20 b)
     8.76786591E-02    2     1000025         5   # BR(~b_2 -> chi_30 b)
     1.10738485E-01    2     1000035         5   # BR(~b_2 -> chi_40 b)
     9.98526730E-02    2    -1000024         6   # BR(~b_2 -> chi_1- t)
     2.17513057E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W-)
DECAY   2000002     1.03580726E+00   # ~u_R
#    BR                NDA      ID1      ID2
     9.73607521E-01    2     1000022         2   # BR(~u_R -> chi_10 u)
     1.80685181E-02    2     1000023         2   # BR(~u_R -> chi_20 u)
     2.17186430E-03    2     1000025         2   # BR(~u_R -> chi_30 u)
     6.15207041E-03    2     1000035         2   # BR(~u_R -> chi_40 u)
DECAY   1000002     5.03714806E+00   # ~u_L
#    BR                NDA      ID1      ID2
     4.30657403E-03    2     1000022         2   # BR(~u_L -> chi_10 u)
     3.12542958E-01    2     1000023         2   # BR(~u_L -> chi_20 u)
     1.50457994E-03    2     1000025         2   # BR(~u_L -> chi_30 u)
     1.49475977E-02    2     1000035         2   # BR(~u_L -> chi_40 u)
     6.50354260E-01    2     1000024         1   # BR(~u_L -> chi_1+ d)
     1.63440307E-02    2     1000037         1   # BR(~u_L -> chi_2+ d)
DECAY   2000004     1.04108034E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.67467509E-01    2     1000022         4   # BR(~c_R -> chi_10 c)
     1.99732544E-02    2     1000023         4   # BR(~c_R -> chi_20 c)
     2.23544740E-03    2     1000025         4   # BR(~c_R -> chi_30 c)
     6.11015856E-03    2     1000035         4   # BR(~c_R -> chi_40 c)
     4.19948525E-03    2     1000024         3   # BR(~c_R -> chi_1+ s)
DECAY   1000004     5.03200699E+00   # ~c_L
#    BR                NDA      ID1      ID2
     4.55749276E-03    2     1000022         4   # BR(~c_L -> chi_10 c)
     3.12442617E-01    2     1000023         4   # BR(~c_L -> chi_20 c)
     1.50072048E-03    2     1000025         4   # BR(~c_L -> chi_30 c)
     1.49770085E-02    2     1000035         4   # BR(~c_L -> chi_40 c)
     6.50149244E-01    2     1000024         3   # BR(~c_L -> chi_1+ s)
     1.63729176E-02    2     1000037         3   # BR(~c_L -> chi_2+ s)
DECAY   1000006     1.80152813E+00   # ~t_1
#    BR                NDA      ID1      ID2
     1.53218099E-01    2     1000022         6   # BR(~t_1 -> chi_10 t)
     9.46613171E-02    2     1000023         6   # BR(~t_1 -> chi_20 t)
     7.38554990E-01    2     1000024         5   # BR(~t_1 -> chi_1+ b)
     1.35655927E-02    2     1000037         5   # BR(~t_1 -> chi_2+ b)
DECAY   2000006     6.60220510E+00   # ~t_2
#    BR                NDA      ID1      ID2
     2.57887089E-02    2     1000022         6   # BR(~t_2 -> chi_10 t)
     8.14706068E-02    2     1000023         6   # BR(~t_2 -> chi_20 t)
     4.43682400E-02    2     1000025         6   # BR(~t_2 -> chi_30 t)
     2.02218631E-01    2     1000035         6   # BR(~t_2 -> chi_40 t)
     2.14674302E-01    2     1000024         5   # BR(~t_2 -> chi_1+ b)
     2.29144884E-01    2     1000037         5   # BR(~t_2 -> chi_2+ b)
     1.71087857E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.12467695E-02    2     1000006        25   # BR(~t_2 -> ~t_1 S0_1)
DECAY   1000024     3.47957212E-05   # chi_1+
#    BR                NDA      ID1      ID2
#    BR                NDA      ID1      ID2       ID3
     5.00341604E-01    3     1000022        -1         2   # BR(chi_1+ -> chi_10 d u)
     4.99658378E-01    3     1000022        -3         4   # BR(chi_1+ -> chi_10 s c)
DECAY   1000037     1.68304102E+00   # chi_2+
#    BR                NDA      ID1      ID2
     7.35220470E-02    2     1000022        24   # BR(chi_2+ -> chi_10 W+)
     3.36206487E-01    2     1000023        24   # BR(chi_2+ -> chi_20 W+)
     1.82245847E-02    2          12  -1000011   # BR(chi_2+ -> nu_1 S^+_4)
     1.00980342E-02    2          14  -1000011   # BR(chi_2+ -> nu_2 S^+_4)
     4.67693803E-03    2          12  -1000013   # BR(chi_2+ -> nu_1 S^+_5)
     1.00097377E-02    2          14  -1000013   # BR(chi_2+ -> nu_2 S^+_5)
     1.36847426E-02    2          16  -1000013   # BR(chi_2+ -> nu_3 S^+_5)
     5.57784436E-03    2          12  -2000015   # BR(chi_2+ -> nu_1 S^+_6)
     8.42640718E-03    2          14  -2000015   # BR(chi_2+ -> nu_2 S^+_6)
     1.49253291E-02    2          16  -2000015   # BR(chi_2+ -> nu_3 S^+_6)
     2.76696760E-01    2     1000024        23   # BR(chi_2+ -> chi_1+ Z)
     7.32492557E-03    2         -15   2000016   # BR(chi_2+ -> tau+ P0_1)
     5.56180527E-03    2         -13   2000014   # BR(chi_2+ -> mu+ P0_2)
     5.55568199E-03    2         -11   2000012   # BR(chi_2+ -> e+ P0_3)
     1.90535990E-01    2     1000024        25   # BR(chi_2+ -> chi_1+ S0_1)
     7.32492557E-03    2         -15   1000016   # BR(chi_2+ -> tau+ S0_2)
     5.56180527E-03    2         -13   1000014   # BR(chi_2+ -> mu+ S0_3)
     5.55568199E-03    2         -11   1000012   # BR(chi_2+ -> e+ S0_4)
#    BR                NDA      ID1      ID2       ID3
     2.86888840E-04    3     1000024        -5         5   # BR(chi_2+ -> chi_1+ b b)
DECAY   1000022     9.25217039E-15   # chi_10
#    BR                NDA      ID1      ID2
#    BR                NDA      ID1      ID2       ID3
#     3.92003698E-03    3         -13         1        -2   # BR(chi_10 -> mu+ d u)
#     3.92003698E-03    3          13        -1         2   # BR(chi_10 -> mu- d u)
#     3.91645431E-03    3         -13         3        -4   # BR(chi_10 -> mu+ s c)
#     3.91645431E-03    3          13        -3         4   # BR(chi_10 -> mu- s c)
#     4.37741885E-03    3         -15         1        -2   # BR(chi_10 -> tau+ d u)
#     4.37741885E-03    3          15        -1         2   # BR(chi_10 -> tau- d u)
#     4.38376873E-03    3         -15         3        -4   # BR(chi_10 -> tau+ s c)
#     4.38376873E-03    3          15        -3         4   # BR(chi_10 -> tau- s c)
#     2.51682461E-04    3          12        -5         5   # BR(chi_10 -> nu_1 b b)
#     1.97373877E-04    3          14        -3         3   # BR(chi_10 -> nu_2 s s)
#     3.51996253E-01    3          14        -5         5   # BR(chi_10 -> nu_2 b b)
#     2.49154759E-03    3          16        -2         2   # BR(chi_10 -> nu_3 u u)
#     2.48237407E-03    3          16        -4         4   # BR(chi_10 -> nu_3 c c)
#     3.15437315E-03    3          16        -1         1   # BR(chi_10 -> nu_3 d d)
#     3.15438029E-03    3          16        -3         3   # BR(chi_10 -> nu_3 s s)
#     3.30382028E-03    3          16        -5         5   # BR(chi_10 -> nu_3 b b)
#     6.20601357E-04    3          12       -11        11   # BR(chi_10 -> nu_1 e+ e-)
#     1.65588008E-03    3          12        13       -11   # BR(chi_10 -> nu_1 mu+ e-)
#     1.65588008E-03    3          12       -13        11   # BR(chi_10 -> nu_1 e+ mu-)
     3.03556695E-03    3          12       -13        13   # BR(chi_10 -> nu_1 mu+ mu-)
#     7.32802308E-02    3          12        15       -11   # BR(chi_10 -> nu_1 tau+ e-)
#     7.32802308E-02    3          12       -15        11   # BR(chi_10 -> nu_1 e+ tau-)
#     7.60404753E-02    3          12        15       -13   # BR(chi_10 -> nu_1 tau+ mu-)
#     7.60404753E-02    3          12       -15        13   # BR(chi_10 -> nu_1 mu+ tau-)
#     2.87368141E-01    3          12       -15        15   # BR(chi_10 -> nu_1 tau+ tau-)
DECAY   1000023     5.30492214E-06   # chi_20
#    BR                NDA      ID1      ID2
     2.39929140E-03    2     1000022        22   # BR(chi_20 -> chi_10 photon)
#    BR                NDA      ID1      ID2       ID3
     1.41475239E-01    3     1000022        -2         2   # BR(chi_20 -> chi_10 u u)
     1.40933956E-01    3     1000022        -4         4   # BR(chi_20 -> chi_10 c c)
     1.96547985E-01    3     1000022        -1         1   # BR(chi_20 -> chi_10 d d)
     1.96544632E-01    3     1000022        -3         3   # BR(chi_20 -> chi_10 s s)
     2.03914256E-01    3     1000022        -5         5   # BR(chi_20 -> chi_10 b b)
     1.81525223E-02    3          12   1000024        13   # BR(chi_20 -> nu_1 mu+ chi_1-)
     1.82633513E-02    3          12  -1000037       -11   # BR(chi_20 -> nu_1 chi_2+ e-)
     5.28443312E-02    3          12   1000037        15   # BR(chi_20 -> nu_1 tau+ chi_2-)
DECAY   1000025     1.40524830E+00   # chi_30
#    BR                NDA      ID1      ID2
     3.04011260E-01    2     1000024       -24   # BR(chi_30 -> chi_1+ W-)
     3.04011260E-01    2    -1000024        24   # BR(chi_30 -> chi_1- W+)
     1.74740813E-03    2         -15   1000015   # BR(chi_30 -> tau+ S^-_1)
     1.74740813E-03    2          15  -1000015   # BR(chi_30 -> tau- S^+_1)
     6.83647259E-04    2         -13   2000013   # BR(chi_30 -> mu+ S^-_2)
     6.83647259E-04    2          13  -2000013   # BR(chi_30 -> mu- S^+_2)
     6.81964637E-04    2         -11   2000011   # BR(chi_30 -> e+ S^-_3)
     6.81964637E-04    2          11  -2000011   # BR(chi_30 -> e- S^+_3)
     2.38648400E-04    2         -11   1000011   # BR(chi_30 -> e+ S^-_4)
     2.38648400E-04    2          11  -1000011   # BR(chi_30 -> e- S^+_4)
     2.46187458E-04    2         -13   1000013   # BR(chi_30 -> mu+ S^-_5)
     2.46187458E-04    2          13  -1000013   # BR(chi_30 -> mu- S^+_5)
     1.97212683E-03    2         -15   2000015   # BR(chi_30 -> tau+ S^-_6)
     1.97212683E-03    2          15  -2000015   # BR(chi_30 -> tau- S^+_6)
     1.29439521E-01    2     1000022        23   # BR(chi_30 -> chi_10 Z)
     2.04144099E-01    2     1000023        23   # BR(chi_30 -> chi_20 Z)
     3.32620009E-04    2          12   2000016   # BR(chi_30 -> nu_1 P0_1)
     5.02486038E-04    2          14   2000016   # BR(chi_30 -> nu_2 P0_1)
     8.90032567E-04    2          16   2000016   # BR(chi_30 -> nu_3 P0_1)
     2.76299719E-04    2          12   2000014   # BR(chi_30 -> nu_1 P0_2)
     5.91218587E-04    2          14   2000014   # BR(chi_30 -> nu_2 P0_2)
     8.08341977E-04    2          16   2000014   # BR(chi_30 -> nu_3 P0_2)
     1.07633017E-03    2          12   2000012   # BR(chi_30 -> nu_1 P0_3)
     5.96447241E-04    2          14   2000012   # BR(chi_30 -> nu_2 P0_3)
     2.45617724E-02    2     1000022        25   # BR(chi_30 -> chi_10 S0_1)
     1.25162647E-02    2     1000023        25   # BR(chi_30 -> chi_20 S0_1)
     3.32620009E-04    2          12   1000016   # BR(chi_30 -> nu_1 S0_2)
     5.02486038E-04    2          14   1000016   # BR(chi_30 -> nu_2 S0_2)
     8.90032567E-04    2          16   1000016   # BR(chi_30 -> nu_3 S0_2)
     2.76299730E-04    2          12   1000014   # BR(chi_30 -> nu_1 S0_3)
     5.91218576E-04    2          14   1000014   # BR(chi_30 -> nu_2 S0_3)
     8.08341978E-04    2          16   1000014   # BR(chi_30 -> nu_3 S0_3)
     1.07633016E-03    2          12   1000012   # BR(chi_30 -> nu_1 S0_4)
     5.96447252E-04    2          14   1000012   # BR(chi_30 -> nu_2 S0_4)
DECAY   1000035     1.90460614E+00   # chi_40
#    BR                NDA      ID1      ID2
     3.08611925E-01    2     1000024       -24   # BR(chi_40 -> chi_1+ W-)
     3.08611925E-01    2    -1000024        24   # BR(chi_40 -> chi_1- W+)
     1.18145440E-03    2         -15   1000015   # BR(chi_40 -> tau+ S^-_1)
     1.18145440E-03    2          15  -1000015   # BR(chi_40 -> tau- S^+_1)
     2.31247583E-03    2         -13   2000013   # BR(chi_40 -> mu+ S^-_2)
     2.31247583E-03    2          13  -2000013   # BR(chi_40 -> mu- S^+_2)
     2.31914718E-03    2         -11   2000011   # BR(chi_40 -> e+ S^-_3)
     2.31914718E-03    2          11  -2000011   # BR(chi_40 -> e- S^+_3)
     4.69754058E-03    2         -11   1000011   # BR(chi_40 -> e+ S^-_4)
     4.69754058E-03    2          11  -1000011   # BR(chi_40 -> e- S^+_4)
     4.70800393E-03    2         -13   1000013   # BR(chi_40 -> mu+ S^-_5)
     4.70800393E-03    2          13  -1000013   # BR(chi_40 -> mu- S^+_5)
     7.01333835E-03    2         -15   2000015   # BR(chi_40 -> tau+ S^-_6)
     7.01333835E-03    2          15  -2000015   # BR(chi_40 -> tau- S^+_6)
     2.69050273E-02    2     1000022        23   # BR(chi_40 -> chi_10 Z)
     2.31742091E-02    2     1000023        23   # BR(chi_40 -> chi_20 Z)
     2.90612823E-03    2          12   2000016   # BR(chi_40 -> nu_1 P0_1)
     4.39026162E-03    2          14   2000016   # BR(chi_40 -> nu_2 P0_1)
     7.77628735E-03    2          16   2000016   # BR(chi_40 -> nu_3 P0_1)
     2.43144091E-03    2          12   2000014   # BR(chi_40 -> nu_1 P0_2)
     5.20273080E-03    2          14   2000014   # BR(chi_40 -> nu_2 P0_2)
     7.11341929E-03    2          16   2000014   # BR(chi_40 -> nu_3 P0_2)
     9.47196326E-03    2          12   2000012   # BR(chi_40 -> nu_1 P0_3)
     5.24887855E-03    2          14   2000012   # BR(chi_40 -> nu_2 P0_3)
     7.31073660E-02    2     1000022        25   # BR(chi_40 -> chi_10 S0_1)
     1.25942029E-01    2     1000023        25   # BR(chi_40 -> chi_20 S0_1)
     2.90612823E-03    2          12   1000016   # BR(chi_40 -> nu_1 S0_2)
     4.39026162E-03    2          14   1000016   # BR(chi_40 -> nu_2 S0_2)
     7.77628735E-03    2          16   1000016   # BR(chi_40 -> nu_3 S0_2)
     2.43144100E-03    2          12   1000014   # BR(chi_40 -> nu_1 S0_3)
     5.20273070E-03    2          14   1000014   # BR(chi_40 -> nu_2 S0_3)
     7.11341930E-03    2          16   1000014   # BR(chi_40 -> nu_3 S0_3)
     9.47196317E-03    2          12   1000012   # BR(chi_40 -> nu_1 S0_4)
     5.24887865E-03    2          14   1000012   # BR(chi_40 -> nu_2 S0_4)
DECAY   1000021     7.14027634E-01   # ~g
#    BR                NDA      ID1      ID2
     3.08949220E-02    2     2000002        -2   # BR(~g -> ~u_R u^*)
     3.08949220E-02    2    -2000002         2   # BR(~g -> ~u_R u)
     2.10760520E-03    2     1000002        -2   # BR(~g -> ~u_L u^*)
     2.10760520E-03    2    -1000002         2   # BR(~g -> ~u_L u)
     3.06900887E-02    2     2000004        -4   # BR(~g -> ~c_R c^*)
     3.06900887E-02    2    -2000004         4   # BR(~g -> ~c_R c)
     2.03823100E-03    2     1000004        -4   # BR(~g -> ~c_L c^*)
     2.03823100E-03    2    -1000004         4   # BR(~g -> ~c_L c)
     2.87139510E-02    2     2000001        -1   # BR(~g -> ~d_R d^*)
     2.87139510E-02    2    -2000001         1   # BR(~g -> ~d_R d)
     2.87251886E-02    2     2000003        -3   # BR(~g -> ~s_R s^*)
     2.87251886E-02    2    -2000003         3   # BR(~g -> ~s_R s)
     3.15637323E-01    2     1000005        -5   # BR(~g -> ~b_1 b^*)
     3.15637323E-01    2    -1000005         5   # BR(~g -> ~b_1 b)
     3.54669674E-02    2     2000005        -5   # BR(~g -> ~b_2 b^*)
     3.54669674E-02    2    -2000005         5   # BR(~g -> ~b_2 b)
     3.80116574E-03    2     1000006        -4   # BR(~g -> ~t_1 c^*)
     3.80116574E-03    2    -1000006         4   # BR(~g -> ~t_1 c)
     1.69304491E-04    2     1000023        21   # BR(~g -> chi_20 gluon)
     3.14316375E-04    2     1000025        21   # BR(~g -> chi_30 gluon)
     2.52046707E-04    2     1000035        21   # BR(~g -> chi_40 gluon)
#    BR                NDA      ID1      ID2       ID3
     7.69450836E-04    3     1000022        -6         6   # BR(~g -> chi_10 t t)
     6.62260253E-04    3     1000022        -1         1   # BR(~g -> chi_10 d d)
     6.62190583E-04    3     1000022        -3         3   # BR(~g -> chi_10 s s)
     6.74764293E-03    3     1000023        -1         1   # BR(~g -> chi_20 d d)
     6.74558334E-03    3     1000023        -3         3   # BR(~g -> chi_20 s s)
     2.64294293E-04    3     1000035        -1         1   # BR(~g -> chi_40 d d)
     2.64370375E-04    3     1000035        -3         3   # BR(~g -> chi_40 s s)
     5.74548036E-03    3     1000024         1        -2   # BR(~g -> chi_1+ d u)
     5.74548036E-03    3    -1000024        -1         2   # BR(~g -> chi_1- d u)
     5.74368871E-03    3     1000024         3        -4   # BR(~g -> chi_1+ s c)
     5.74368871E-03    3    -1000024        -3         4   # BR(~g -> chi_1- s c)
     1.37442408E-03    3     1000024         5        -6   # BR(~g -> chi_1+ b t)
     1.37442408E-03    3    -1000024        -5         6   # BR(~g -> chi_1- b t)
     2.88398276E-04    3     1000037         1        -2   # BR(~g -> chi_2+ d u)
     2.88398276E-04    3    -1000037        -1         2   # BR(~g -> chi_2- d u)
     2.88482915E-04    3     1000037         3        -4   # BR(~g -> chi_2+ s c)
     2.88482915E-04    3    -1000037        -3         4   # BR(~g -> chi_2- s c)
DECAY        25     2.24003094E-03   # S0_1      
#    BR                NDA      ID1      ID2
     4.62027292E-04    2          -3         3   # BR(S0_1 -> s s)
     8.32743502E-01    2          -5         5   # BR(S0_1 -> b b)
     4.08851134E-02    2          -4         4   # BR(S0_1 -> c c)
     4.38677114E-04    2          13       -13   # BR(S0_1 -> mu- mu+)
     1.23915157E-01    2          15       -15   # BR(S0_1 -> tau- tau+)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     3.10736991E-04    3          23        12       -12   # BR(S0_1 -> Z0 nu_i nu_i)
     5.43789735E-05    3          23        11       -11   # BR(S0_1 -> Z0 e- e+)
     5.43789735E-05    3          23        13       -13   # BR(S0_1 -> Z0 mu- mu+)
     4.66105487E-05    3          23        15       -15   # BR(S0_1 -> Z0 tau- tau+)
     2.17515894E-04    3          23         1        -1   # BR(S0_1 -> Z0 d d)
     2.17515894E-04    3          23         3        -3   # BR(S0_1 -> Z0 s s)
     2.17515894E-04    3          23         5        -5   # BR(S0_1 -> Z0 b b)
     2.17515894E-04    3          23         2        -2   # BR(S0_1 -> Z0 u u)
     2.17515894E-04    3          23         4        -4   # BR(S0_1 -> Z0 c c)
DECAY   1000016     1.30960971E+00   # S0_2      
#    BR                NDA      ID1      ID2
     4.34624495E-02    2          12   1000022   # BR(S0_2 -> nu_1 chi_10)
     4.07065635E-02    2          12   1000023   # BR(S0_2 -> nu_1 chi_20)
     6.56583292E-02    2          14   1000022   # BR(S0_2 -> nu_2 chi_10)
     6.14950371E-02    2          14   1000023   # BR(S0_2 -> nu_2 chi_20)
     1.16297861E-01    2          16   1000022   # BR(S0_2 -> nu_3 chi_10)
     1.08923595E-01    2          16   1000023   # BR(S0_2 -> nu_3 chi_20)
     2.81728080E-01    2          15   1000024   # BR(S0_2 -> tau- chi_1+)
     2.81728080E-01    2         -15  -1000024   # BR(S0_2 -> tau+ chi_1-)
DECAY   1000014     1.32635804E+00   # S0_3      
#    BR                NDA      ID1      ID2
     3.69683434E-02    2          12   1000022   # BR(S0_3 -> nu_1 chi_10)
     3.49384074E-02    2          12   1000023   # BR(S0_3 -> nu_1 chi_20)
     7.91038462E-02    2          14   1000022   # BR(S0_3 -> nu_2 chi_10)
     7.47602450E-02    2          14   1000023   # BR(S0_3 -> nu_2 chi_20)
     1.08154517E-01    2          16   1000022   # BR(S0_3 -> nu_3 chi_10)
     1.02215740E-01    2          16   1000023   # BR(S0_3 -> nu_3 chi_20)
     2.81929448E-01    2          13   1000024   # BR(S0_3 -> mu- chi_1+)
     2.81929448E-01    2         -13  -1000024   # BR(S0_3 -> mu+ chi_1-)
DECAY   1000012     1.32641717E+00   # S0_4      
#    BR                NDA      ID1      ID2
     1.44023071E-01    2          12   1000022   # BR(S0_4 -> nu_1 chi_10)
     1.36119057E-01    2          12   1000023   # BR(S0_4 -> nu_1 chi_20)
     7.98102367E-02    2          14   1000022   # BR(S0_4 -> nu_2 chi_10)
     7.54302354E-02    2          14   1000023   # BR(S0_4 -> nu_2 chi_20)
     3.89263039E-04    2          16   1000022   # BR(S0_4 -> nu_3 chi_10)
     3.67900208E-04    2          16   1000023   # BR(S0_4 -> nu_3 chi_20)
     2.81930116E-01    2          11   1000024   # BR(S0_4 -> e- chi_1+)
     2.81930116E-01    2         -11  -1000024   # BR(S0_4 -> e+ chi_1-)
DECAY        35     9.06340651E-01   # S0_5      
#    BR                NDA      ID1      ID2
     3.03468855E-04    2          -3         3   # BR(S0_5 -> s s)
     5.49063810E-01    2          -5         5   # BR(S0_5 -> b b)
     2.31945426E-02    2          -6         6   # BR(S0_5 -> t t)
     3.05656065E-02    2     1000022   1000022   # BR(S0_5 -> chi_10 chi_10)
     9.57598961E-02    2     1000022   1000023   # BR(S0_5 -> chi_10 chi_20)
     5.62090132E-02    2     1000023   1000023   # BR(S0_5 -> chi_20 chi_20)
     2.84734822E-04    2          13       -13   # BR(S0_5 -> mu- mu+)
     8.05485297E-02    2          15       -15   # BR(S0_5 -> tau- tau+)
     1.62909285E-01    2    -1000024   1000024   # BR(S0_5 -> chi_1- chi_1+)
     1.15555747E-03    2          23        23   # BR(S0_5 -> Z0 Z0)
DECAY   2000016     1.30960971E+00   # P0_1      
#    BR                NDA      ID1      ID2
     4.34624495E-02    2          12   1000022   # BR(P0_1 -> nu_1 chi_10)
     4.07065635E-02    2          12   1000023   # BR(P0_1 -> nu_1 chi_20)
     6.56583292E-02    2          14   1000022   # BR(P0_1 -> nu_2 chi_10)
     6.14950371E-02    2          14   1000023   # BR(P0_1 -> nu_2 chi_20)
     1.16297861E-01    2          16   1000022   # BR(P0_1 -> nu_3 chi_10)
     1.08923595E-01    2          16   1000023   # BR(P0_1 -> nu_3 chi_20)
     2.81728080E-01    2          15   1000024   # BR(P0_1 -> tau- chi_1+)
     2.81728080E-01    2         -15  -1000024   # BR(P0_1 -> tau+ chi_1-)
DECAY   2000014     1.32635804E+00   # P0_2      
#    BR                NDA      ID1      ID2
     3.69683420E-02    2          12   1000022   # BR(P0_2 -> nu_1 chi_10)
     3.49384061E-02    2          12   1000023   # BR(P0_2 -> nu_1 chi_20)
     7.91038477E-02    2          14   1000022   # BR(P0_2 -> nu_2 chi_10)
     7.47602465E-02    2          14   1000023   # BR(P0_2 -> nu_2 chi_20)
     1.08154517E-01    2          16   1000022   # BR(P0_2 -> nu_3 chi_10)
     1.02215740E-01    2          16   1000023   # BR(P0_2 -> nu_3 chi_20)
     2.81929448E-01    2          13   1000024   # BR(P0_2 -> mu- chi_1+)
     2.81929448E-01    2         -13  -1000024   # BR(P0_2 -> mu+ chi_1-)
DECAY   2000012     1.32641717E+00   # P0_3      
#    BR                NDA      ID1      ID2
     1.44023072E-01    2          12   1000022   # BR(P0_3 -> nu_1 chi_10)
     1.36119058E-01    2          12   1000023   # BR(P0_3 -> nu_1 chi_20)
     7.98102352E-02    2          14   1000022   # BR(P0_3 -> nu_2 chi_10)
     7.54302340E-02    2          14   1000023   # BR(P0_3 -> nu_2 chi_20)
     3.89263161E-04    2          16   1000022   # BR(P0_3 -> nu_3 chi_10)
     3.67900323E-04    2          16   1000023   # BR(P0_3 -> nu_3 chi_20)
     2.81930116E-01    2          11   1000024   # BR(P0_3 -> e- chi_1+)
     2.81930116E-01    2         -11  -1000024   # BR(P0_3 -> e+ chi_1-)
DECAY        36     1.45432958E+00   # P0_4      
#    BR                NDA      ID1      ID2
     1.89511074E-04    2          -3         3   # BR(P0_4 -> s s)
     3.42930138E-01    2          -5         5   # BR(P0_4 -> b b)
     7.65059507E-02    2          -6         6   # BR(P0_4 -> t t)
     2.67332471E-02    2     1000022   1000022   # BR(P0_4 -> chi_10 chi_10)
     1.07366109E-01    2     1000022   1000023   # BR(P0_4 -> chi_10 chi_20)
     1.03971062E-01    2     1000023   1000023   # BR(P0_4 -> chi_20 chi_20)
     1.77813722E-04    2          13       -13   # BR(P0_4 -> mu- mu+)
     5.03062985E-02    2          15       -15   # BR(P0_4 -> tau- tau+)
     2.90553295E-01    2    -1000024   1000024   # BR(P0_4 -> chi_1- chi_1+)
     1.26375050E-03    2          23        25   # BR(P0_4 -> Z0 S0_1)
DECAY   1000015     8.52228363E-01   # S^-_1     
#    BR                NDA      ID1      ID2
     8.55285692E-01    2          15   1000022   # BR(S^-_1 -> tau- chi_10)
     5.73601432E-02    2          15   1000023   # BR(S^-_1 -> tau- chi_20)
     1.68425515E-02    2    -1000024        12   # BR(S^-_1 -> chi_1- nu_1)
     2.54439082E-02    2    -1000024        14   # BR(S^-_1 -> chi_1- nu_2)
     4.50677015E-02    2    -1000024        16   # BR(S^-_1 -> chi_1- nu_3)
DECAY   2000013     8.38967398E-01   # S^-_2     
#    BR                NDA      ID1      ID2
     9.89809790E-01    2          13   1000022   # BR(S^-_2 -> mu- chi_10)
     9.65217149E-03    2          13   1000023   # BR(S^-_2 -> mu- chi_20)
     1.89822635E-04    2    -1000024        14   # BR(S^-_2 -> chi_1- nu_2)
     2.59519171E-04    2    -1000024        16   # BR(S^-_2 -> chi_1- nu_3)
DECAY   2000011     8.38796510E-01   # S^-_3     
#    BR                NDA      ID1      ID2
     9.90647694E-01    2          11   1000022   # BR(S^-_3 -> e- chi_10)
     9.35229383E-03    2          11   1000023   # BR(S^-_3 -> e- chi_20)
DECAY   1000011     1.40681262E+00   # S^-_4     
#    BR                NDA      ID1      ID2
     1.30390310E-01    2          11   1000022   # BR(S^-_4 -> e- chi_10)
     3.34731775E-01    2          11   1000023   # BR(S^-_4 -> e- chi_20)
     3.43576438E-01    2    -1000024        12   # BR(S^-_4 -> chi_1- nu_1)
     1.90371784E-01    2    -1000024        14   # BR(S^-_4 -> chi_1- nu_2)
     9.29691796E-04    2    -1000024        16   # BR(S^-_4 -> chi_1- nu_3)
DECAY   1000013     1.40644648E+00   # S^-_5     
#    BR                NDA      ID1      ID2
     1.30752876E-01    2          13   1000022   # BR(S^-_5 -> mu- chi_10)
     3.34609827E-01    2          13   1000023   # BR(S^-_5 -> mu- chi_20)
     8.81332565E-02    2    -1000024        12   # BR(S^-_5 -> chi_1- nu_1)
     1.88625720E-01    2    -1000024        14   # BR(S^-_5 -> chi_1- nu_2)
     2.57878320E-01    2    -1000024        16   # BR(S^-_5 -> chi_1- nu_3)
DECAY   2000015     1.33644637E+00   # S^-_6     
#    BR                NDA      ID1      ID2
     1.98602033E-01    2          15   1000022   # BR(S^-_6 -> tau- chi_10)
     3.12475814E-01    2          15   1000023   # BR(S^-_6 -> tau- chi_20)
     9.42679284E-02    2    -1000024        12   # BR(S^-_6 -> chi_1- nu_1)
     1.42409850E-01    2    -1000024        14   # BR(S^-_6 -> chi_1- nu_2)
     2.52244374E-01    2    -1000024        16   # BR(S^-_6 -> chi_1- nu_3)
DECAY       -37     2.89361160E-01   # S^-_7     
#    BR                NDA      ID1      ID2
     1.50885557E-04    2          13        12   # BR(S^-_7 -> mu- nu_1)
     3.22914469E-04    2          13        14   # BR(S^-_7 -> mu- nu_2)
     4.41477891E-04    2          13        16   # BR(S^-_7 -> mu- nu_3)
     4.99269799E-02    2          15        12   # BR(S^-_7 -> tau- nu_1)
     7.54242913E-02    2          15        14   # BR(S^-_7 -> tau- nu_2)
     1.33595805E-01    2          15        16   # BR(S^-_7 -> tau- nu_3)
     7.22185470E-01    2    -1000024   1000022   # BR(S^-_7 -> chi_1- chi_10)
     1.06681273E-02    2    -1000024   1000023   # BR(S^-_7 -> chi_1- chi_20)
Block SPhenoLowEnergy  # low energy observables
    1    3.18679915E-04   # BR(b -> s gamma)
    2    1.90232349E-06   # BR(b -> s mu+ mu-)
    3    4.71774209E-05   # BR(b -> s nu nu)
    4    4.48732489E-09   # BR(Bs -> mu+ mu-)
    5    7.69053138E-05   # BR(B_u -> tau nu)
    6    2.31839446E-01   # |Delta(M_Bd)| [ps^-1] 
    7    1.67781521E+01   # |Delta(M_Bs)| [ps^-1] 
   10    5.69318123E-14   # Delta(g-2)_electron
   11    2.72146443E-09   # Delta(g-2)_muon
   12    7.05315827E-07   # Delta(g-2)_tau
   13    0.00000000E+00   # electric dipole moment of the electron
   14    0.00000000E+00   # electric dipole moment of the muon
   15    0.00000000E+00   # electric dipole moment of the tau
   16    0.00000000E+00   # Br(mu -> e gamma)
   17    0.00000000E+00   # Br(tau -> e gamma)
   18    0.00000000E+00   # Br(tau -> mu gamma)
   19    0.00000000E+00   # Br(mu -> 3 e)
   20    0.00000000E+00   # Br(tau -> 3 e)
   21    0.00000000E+00   # Br(tau -> 3 mu)
   30    4.02831246E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
