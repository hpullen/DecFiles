#!/usr/bin/env python
import os
import re
import logging
import subprocess


SCRIPT_ROOT = os.path.abspath(os.path.dirname(__file__))


def generator_options(dkopts):
    '''
    Return the generator options file name for a given decay file option.
    '''
    # FIXME: this could become an option file
    default = '$LBPYTHIA8ROOT/options/Pythia8.py'
    evt_type = os.path.splitext(os.path.basename(dkopts))[0]
    base = evt_type[:2]
    sub = evt_type[6]
    if base == '14':
        return '$LBBCVEGPYROOT/options/BcVegPyPythia8.py'
    elif base == '26':
        if sub in ('5', '6'):
            return '$LBGENXICCROOT/options/GenXiccPythia8.py'
        elif sub in ('8', '9'):
            return '$LBPYTHIA8ROOT/options/Pythia8.py'
    elif base == '16':
        if sub == '6':
            return '$LBGENXICCROOT/options/GenXiccPythia8.py'
    elif base[0] == '4':
        return productionTool(dkopts) or default
    # default fallback
    return default


def productionTool(dkopts):
    '''
    Extract production tool from a decay options file.
    '''
    logging.getLogger('productionTool').debug('opening %f', dkopts)
    for line in open(dkopts):
        if 'Generation().Special.addTool' in line:
            if 'Pythia8Production' in line:
                return '$LBPYTHIA8ROOT/options/Pythia8.py'
            elif 'OniaPairsProduction' in line:
                # FIXME: Not perfect, but best without significant changes to
                #        script
                return '$DECFILESROOT/options/LbOniaPsi1S1S.py'
            elif 'PowhegProduction' in line:
                return '$LBPOWHEGROOT/options/Powheg.py'
        if 'Generation.AlpGenDict' in line:
            return '$LBALPGENROOT/options/AlpGen.py'
        match = re.search(
            r'''importOptions\s*\(\s*['"]([^'"]+)['"]\s*\)''', line
        )
        if match:
            # try recursion
            dkopts = os.path.expandvars(match.group(1))
            tool = productionTool(dkopts)
            if tool:
                return tool  # recursion successful
    return None  # nothing found


def getCPUTime(dkopts):
    '''
    Return the CPU time declared in the .dec file of an option file.
    '''
    log = logging.getLogger('getCPUTime')
    log.debug('looking for source file in %s', dkopts)
    # find original .dec file
    m = re.search(r'''EvtGenDecay\.UserDecayFile\s*=\s*['"]([^'"]+)['"]''',
                  open(dkopts).read())
    if not m:
        log.warning('missing dec file name in %s, assume 60s', dkopts)
        return 60
    decfile = os.path.expandvars(m.group(1))
    log.debug('parsing %s', decfile)
    # we have to match any variant of "CPUTime : [<] 123 [min|m|s|sec|minute]"
    m = re.search(r'CPUTime\s*:?\s*<?\s*([0-9]+(?:\.[0-9]*)?)\s*(m|s)',
                  open(decfile).read(), re.IGNORECASE)
    if not m:
        log.warning('missing CPUTime declaration %s, assume 60s', decfile)
        return 60

    cputime = int(float(m.group(1)) * (60 if m.group(2).lower() == 'm' else 1))
    log.debug('found CPUTime %ds', cputime)
    return cputime


class Popen(subprocess.Popen):
    '''
    Extension to subprocess.Popen that adds a timeout option (in seconds).
    '''
    def __init__(self, *args, **kwargs):
        self.timeout = kwargs.pop('timeout', -1)
        self.timer = None
        super(Popen, self).__init__(*args, **kwargs)
        if self.timeout > 0:
            from threading import Timer
            self.timer = Timer(self.timeout, self._terminate_timeout)
            self.timer.start()

    def _terminate_timeout(self):
        logging.warning('terminating process %d after %d seconds',
                        self.pid, self.timeout)
        self.timer.cancel()
        self.terminate()

    def wait(self):
        try:
            return super(Popen, self).wait()
        finally:
            if self.timer:
                self.timer.cancel()
                self.timer = None

    def communicate(self, input=None):
        try:
            return super(Popen, self).communicate(input)
        finally:
            if self.timer:
                self.timer.cancel()
                self.timer = None


def run_gauss(dkopts, nevts=5, timeout_seconds=-1):
    '''
    Run Gauss (the environment must be ready) for the specified decay options
    file.

    Return (exit_code, stdout, stderr)
    '''
    from datetime import datetime
    try:
        from shlex import quote  # Python 3 version
    except ImportError:
        from pipes import quote  # Python 2 version

    log = logging.getLogger('run_gauss')

    gaussopts = os.environ['GAUSSOPTS']
    gaussroot = os.environ['GAUSSROOT']
    cmdline = ['gaudirun.py',
               os.path.join(gaussopts, 'Gauss-2016.py'),
               dkopts, os.path.expandvars(generator_options(dkopts)),
               os.path.join(gaussopts, 'GenStandAlone.py'),
               os.path.join(gaussroot, 'tests', 'options',
                            'testGauss-NoOutput.py'),
               os.path.join(SCRIPT_ROOT, 'options', 'decfiles_test_opts.py'),
               '--option', ('from Configurables import LHCbApp;'
                            'LHCbApp(EvtMax={})').format(nevts)]

    start = datetime.now()
    log.info('running: %s', ' '.join(quote(token) for token in cmdline))
    if timeout_seconds > 0:
        log.info('timeout set to %d s', timeout_seconds)
    proc = Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                 timeout=timeout_seconds)

    stdout, stderr = proc.communicate()
    log.info('process completed with exit code %d', proc.returncode)
    log.debug('elapsed time: %s', datetime.now() - start)
    return (proc.returncode, stdout, stderr)


def computeNEvtsAndTimeout(dkopts, nevts=None, timeout=None):
    '''
    Deduce correct nevts and timeout parameters from option file and optional
    requested values.

    Return (nevts, timeout)
    '''
    if nevts and timeout:
        # everything specified, no need to guess
        return nevts, timeout

    SAFETY_FACTOR = 1.3  # to take into account fluctuations
    INITIALIZATION = 240  # seconds

    s_per_evt = getCPUTime(dkopts)
    if nevts:
        timeout = int(SAFETY_FACTOR * nevts * s_per_evt) + INITIALIZATION
    elif timeout:
        nevts = (timeout - INITIALIZATION) / (SAFETY_FACTOR * s_per_evt)
    else:
        # FIXME this is the original logic of old test script
        nevts = 30 * 60 / s_per_evt  # events in 30 min
        # we do not want more than 10 or less than 4
        nevts = max(min(nevts, 10), 4)
        timeout = int(SAFETY_FACTOR * nevts * s_per_evt) + INITIALIZATION
        # don't allow more than 2h
        timeout = min(timeout, 120 * 60)

    return nevts, timeout


def main():
    import sys
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('dkopts', help='decay options file')
    parser.add_argument('-n', '--nevts', type=int,
                        help='number of events to generate '
                        '[default guessed from options/timeout]')
    parser.add_argument('-t', '--timeout', type=int,
                        help='number of seconds allowed to the process '
                        '[default guessed from options/nevts]')
    parser.add_argument('-q', '--quiet', action='store_const',
                        const=logging.ERROR, dest='log_level',
                        help='less output')
    parser.add_argument('-v', '--verbose', action='store_const',
                        const=logging.INFO, dest='log_level',
                        help='be more verbose')
    parser.add_argument('-d', '--debug', action='store_const',
                        const=logging.DEBUG, dest='log_level',
                        help='enable debug output')
    parser.add_argument('-C', '--workdir',
                        help='working directory where to run Gauss and save '
                        'log files')
    parser.set_defaults(log_level=logging.WARNING)

    args = parser.parse_args()
    logging.basicConfig(level=args.log_level)

    args.dkopts = os.path.abspath(args.dkopts)

    if args.workdir:
        logging.debug('using working directory %s', args.workdir)
        os.chdir(args.workdir)

    args.nevts, args.timeout = computeNEvtsAndTimeout(
        args.dkopts, args.nevts, args.timeout)
    retcode, stdout, stderr = run_gauss(args.dkopts, args.nevts, args.timeout)
    with open('stdout', 'w') as f:
        f.write(stdout)
    with open('stderr', 'w') as f:
        f.write(stderr)
    with open('exitcode', 'w') as f:
        f.write(str(retcode))

    # check result
    try:
        assert retcode == 0, 'retcode {}'.format(retcode)
        assert 'Application Manager Terminated successfully' in stdout, \
            'missing success message'
        assert '{} events processed'.format(args.nevts) in stdout, \
            'wrong number of generated events'
        assert 'ERROR No cuts are specified for the selected particle' \
            not in stdout, 'misconfigured LoKi cuts'
        assert not stderr.strip(), 'non empty stderr'
    except AssertionError as msg:
        logging.error('job failed with %s', msg)
        sys.exit(1)
    logging.info('success')


if __name__ == '__main__':
    main()
